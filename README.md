# RayPy

RayPy a ray tracer engine written in python.

### Dependencies
* Python 3.6
* Numpy
* Pillow

### Features
* Camera position
* Multiple spot light
* Sphere
* Triangular mesh
* Glossy surface
* Texture
* Bump mapping
* Reflection
* Refraction
* Object file
* Antialiasing

Some outputs:

![Image 1](render/1.png)

![Image 2](render/2.png)

![Image 3](render/3.png)

![Image 4](render/4.png)